CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 The `Address decoupled` module is basically an API expose for the address
 module.

 The module consists of a service that connects to the address services for
 building an address format via API, validating any address, listing all the
 available countries, and saving an address to any entity.

 The module has several API endpoints that work with the address logic. As a
 note, there is no support yet for the zone or country field types, but only for
 the address because it's a common usage in most of the cases. Here are the
 available endpoints:

 - Endpoint: `/address-decoupled/api/countries-list/{country_codes}: GET`
 - Params:
   - `country_codes: (string), (optional)`
 ---
 - Exampple request: `/address-decoupled/api/countries-list/{"AU":"AU", "UA":"UA"}`
 - Payload:
   - `country_codes: {"AU": "AU", "UA": "UA"}`
 - Example response:
 ```
 {
    "AU": {
      "countryCode": "AU",
      "name": "Australia",
      "threeLetterCode": "AUS",
      "numericCode": "036",
      "currencyCode": "AUD",
      "locale": "en"
    },
    "UA": {
      "countryCode": "UA",
      "name": "Ukraine",
      "threeLetterCode": "UKR",
      "numericCode": "804",
      "currencyCode": "UAH",
      "locale": "en"
    }
 }
 ```

 - Endpoint: `/address-decoupled/api/country-data/{country_code}: GET`
 - Params:
   - `country_code: (string), (required)`
 ---
 - Example request: `/address-decoupled/api/country-data/AU`
 - Example response 200:
 ```
 {
    "used_fields": [
      "administrativeArea",
      "locality",
      "postalCode",
      "addressLine1",
      "addressLine2",
      "organization",
      "givenName",
      "familyName"
    ],
    "subdivision_fields": [
      "administrativeArea",
      "locality"
    ],
    "required_fields": [
      "addressLine1",
      "locality",
      "administrativeArea",
      "postalCode",
      "givenName",
      "familyName"
    ],
    "uppercase_fields": [
      "locality",
      "administrativeArea"
    ],
    "postal_code_type": "postal",
    "postal_code_pattern": "\\d{4}",
    "postal_code_prefix": null,
    "field_labels": {
      "givenName": "First name",
      "additionalName": "Middle name",
      "familyName": "Last name",
      "organization": "Company",
      "addressLine1": "Street address",
      "addressLine2": "Street address line 2",
      "postalCode": "Postal code",
      "sortingCode": "Cedex",
      "dependentLocality": null,
      "locality": "Suburb",
      "administrativeArea": "State"
    },
    "subdivisions": {
      "administrativeArea": {
        "ACT": {
          "countryCode": "AU",
          "code": "ACT",
          "localCode": null,
          "name": "Australian Capital Territory",
          "localName": null,
          "isoCode": "AU-ACT",
          "locale": null
        },
        "JBT": {
          "countryCode": "AU",
          "code": "JBT",
          "localCode": null,
          "name": "Jervis Bay Territory",
          "localName": null,
          "isoCode": null,
          "locale": null
        },
        "NSW": {
          "countryCode": "AU",
          "code": "NSW",
          "localCode": null,
          "name": "New South Wales",
          "localName": null,
          "isoCode": "AU-NSW",
          "locale": null
        },
        "NT": {
          "countryCode": "AU",
          "code": "NT",
          "localCode": null,
          "name": "Northern Territory",
          "localName": null,
          "isoCode": "AU-NT",
          "locale": null
        },
        "QLD": {
          "countryCode": "AU",
          "code": "QLD",
          "localCode": null,
          "name": "Queensland",
          "localName": null,
          "isoCode": "AU-QLD",
          "locale": null
        },
        "SA": {
          "countryCode": "AU",
          "code": "SA",
          "localCode": null,
          "name": "South Australia",
          "localName": null,
          "isoCode": "AU-SA",
          "locale": null
        },
        "TAS": {
          "countryCode": "AU",
          "code": "TAS",
          "localCode": null,
          "name": "Tasmania",
          "localName": null,
          "isoCode": "AU-TAS",
          "locale": null
        },
        "VIC": {
          "countryCode": "AU",
          "code": "VIC",
          "localCode": null,
          "name": "Victoria",
          "localName": null,
          "isoCode": "AU-VIC",
          "locale": null
        },
        "WA": {
          "countryCode": "AU",
          "code": "WA",
          "localCode": null,
          "name": "Western Australia",
          "localName": null,
          "isoCode": "AU-WA",
          "locale": null
        }
      }
    }
 }
 ```

 - Endpoint: `/address-decoupled/api/save-address/{address_data}/{entity_id}/{entity_type}/{field}: POST`
 - Params:
   - `address_data: (string), (required)`
   - `entity_id: (int), (required)`
   - `entity_type: (string), (required)`
   - `field: (string), (required)`
 ---
 - Example request: `/address-decoupled/api/save-address/{"countryCode":"CN","administrativeArea":"Jilin Sheng","locality":"Siping Shi","dependentLocality":"Lishu Xian","postalCode":"134000","sortingCode":null,"addressLine1":"A cool address here too","addressLine2":"","organization":"","givenName":"Some name here","additionalName":null,"familyName":"Family Name"}/1/node/field_address`
 - Payload:
   - `address_data: {"countryCode":"CN","administrativeArea":"Jilin Sheng","locality":"Siping Shi","dependentLocality":"Lishu Xian","postalCode":"134000","sortingCode":null,"addressLine1":"A cool address here too","addressLine2":"","organization":"","givenName":"Some name here","additionalName":null,"familyName":"Family Name"}`
   - `entity_id: 1`
   - `entity_type: node`
   - `field: field_address`
 - Example response:
 ```
 {
    "message": "The address was saved with success"
 }
 ```

 - Endpoint: `/address-decoupled/api/validate-address/{address_data}: GET`
 - Params:
   - `address_data: (string), (required)`
 ---
 Example request: `/address-decoupled/api/validate-address/{"countryCode":"CN","administrativeArea":"Jilin Sheng","locality":"Siping Shi","dependentLocality":"Lishu Xian","postalCode":"134000","sortingCode":null,"addressLine1":"A cool address here too","addressLine2":"","organization":"","givenName":"Some  name here","additionalName":null,"familyName":"Family Name"}`
 - Paylaod:
   - `address_data: {"countryCode":"CN","administrativeArea":"Jilin Sheng","locality":"Siping Shi","dependentLocality":"Lishu Xian","postalCode":"134000","sortingCode":null,"addressLine1":"A cool address here too","addressLine2":"","organization":"","givenName":"Some  name here","additionalName":null,"familyName":"Family Name"}`
 - Example response:
 ```
 {
    "message": "The given address is a valid one."
 }
 ```

  - Endpoint: `/address-decoupled/api/store/supported-countries/{store}: GET`
 - Params:
   - `store: (int), (required)`
 ---
 Example request: `/address-decoupled/api/store/supported-countries/1`
 - Paylaod:
   - `store: 1`
 - Example response:
 ```
  {
    "shipping_countries": {
      "CA": {
        "countryCode": "CA",
        "name": "Canada",
        "threeLetterCode": "CAN",
        "numericCode": "124",
        "currencyCode": "CAD",
        "locale": "en"
      },
      "US": {
        "countryCode": "US",
        "name": "United States",
        "threeLetterCode": "USA",
        "numericCode": "840",
        "currencyCode": "USD",
        "locale": "en"
      }
    },
    "billing_countries": {
      "CA": {
        "countryCode": "CA",
        "name": "Canada",
        "threeLetterCode": "CAN",
        "numericCode": "124",
        "currencyCode": "CAD",
        "locale": "en"
      },
      "US": {
        "countryCode": "US",
        "name": "United States",
        "threeLetterCode": "USA",
        "numericCode": "840",
        "currencyCode": "USD",
        "locale": "en"
      }
    }
 }
 ```

REQUIREMENTS
-------------------

 * drupal:address

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/extending-drupal/installing-modules for further
   information.

CONFIGURATION
-------------

 * Once the module gets installed make sure to configure the user permissions for
 the corresponding user roles to access the new REST resources from this module.
 The permissions can be found under the `RESTful Web Services`. Here are
 the permissions:
   - `Access GET on Address decoupled - Countries list resource`
   - `Access GET on Address decoupled - Country data resource`
   - `Access GET on Address decoupled - Validate address resource`
   - `Access POST on Address decoupled - Save address resource`
   - `Access GET on Address decoupled - Store Supported Countries resource`

TROUBLESHOOTING
---------------

 * For error handling, check the `/admin/reports/dblog`, channel `address_decoupled_rest`.


MAINTAINERS
-----------

Current maintainers:

* [Vesterli Andrei](https://www.drupal.org/u/andreivesterli)
* [Miloš Denčev (thenchev)](https://www.drupal.org/u/thenchev)
* [Dylan Fontaine (dylf)](https://www.drupal.org/u/dylf)
