<?php

namespace Drupal\address_decoupled_commerce\Plugin\rest\resource;

use Drupal\address_decoupled\Services\AddressDecoupledInterface;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide the Address decoupled Commerce Store supported countries resource.
 *
 * @RestResource(
 *   id = "address_decoupled_store_supported_countries",
 *   label = @Translation("Address decoupled - Store Supported Countries"),
 *   uri_paths = {
 *     "canonical" = "/address-decoupled/api/store/supported-countries",
 *   }
 * )
 */
class AddressDecoupledStoreSupportedCountriesResource extends ResourceBase {

  /**
   * The address decoupled.
   *
   * @var \Drupal\address_decoupled\Services\AddressDecoupledInterface
   */
  protected AddressDecoupledInterface $addressDecoupled;

  /**
   * The current commerce store.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected CurrentStoreInterface $currentStore;

  /**
   * Constructs the AddressDecoupledStoreSupportedCountriesResource object.
   *
   * @param array $config
   *   Config array which contains the information about the plugin instance.
   * @param string $module_id
   *   The module_id for the plugin instance.
   * @param mixed $module_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\address_decoupled\Services\AddressDecoupledInterface $address_decoupled
   *   The address decoupled service.
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store.
   */
  public function __construct(
    array $config,
    $module_id,
    $module_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AddressDecoupledInterface $address_decoupled,
    CurrentStoreInterface $current_store
  ) {
    parent::__construct($config, $module_id, $module_definition, $serializer_formats, $logger);
    $this->addressDecoupled = $address_decoupled;
    $this->currentStore = $current_store;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $config, $module_id, $module_definition) {
    return new static(
      $config,
      $module_id,
      $module_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('address_decoupled_rest'),
      $container->get('address_decoupled'),
      $container->get('commerce_store.current_store'),
    );
  }

  /**
   * Get all the supported countries from a commmerce Store entity.
   *
   * @return \Drupal\rest\ResourceResponse
   *   Returns the rest response with the store's supported countries data.
   */
  public function get(): ResourceResponse {
    // Get current commerce store object (if possible).
    $store_object = $this->currentStore->getStore();

    if (!$store_object instanceof StoreInterface) {
      return new ResourceResponse(['message' => 'There are no any supported countries for this store.']);
    }

    $shipping_countries = [];
    if ($store_object->hasField('shipping_countries')) {
      $shipping_countries = array_column($store_object->get('shipping_countries')->getValue(), 'value');
      $shipping_countries = $this->addressDecoupled->getCountriesList($shipping_countries);
    }

    $billing_countries = [];
    if ($store_object->hasField('billing_countries')) {
      $billing_countries = array_column($store_object->get('billing_countries')->getValue(), 'value');
      $billing_countries = $this->addressDecoupled->getCountriesList($billing_countries);
    }

    // Set the cache tag dependency.
    $response = new ResourceResponse([
      'shipping_countries' => $shipping_countries,
      'billing_countries' => $billing_countries,
    ]);
    $response->addCacheableDependency($store_object);
    // Handles the case when there is a multi-domain setup.
    $response->addCacheableDependency('url');
    return $response;
  }

}
