<?php

namespace Drupal\address_decoupled\Plugin\rest\resource;

use Drupal\address_decoupled\Services\AddressDecoupledInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide the Address decoupled Commerce Store supported countries resource.
 *
 * @RestResource(
 *   id = "address_decoupled_store_supported_countries",
 *   label = @Translation("Address decoupled - Store Supported Countries"),
 *   uri_paths = {
 *     "canonical" = "/address-decoupled/api/store/supported-countries/{store}",
 *   }
 * )
 */
class AddressDecoupledStoreSupportedCountriesResource extends ResourceBase {

  /**
   * The address decoupled.
   *
   * @var \Drupal\address_decoupled\Services\AddressDecoupledInterface
   */
  protected AddressDecoupledInterface $addressDecoupled;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs a AddressDecoupledStoreSupportedCountriesResource object.
   *
   * @param array $config
   *   Config array which contains the information about the plugin instance.
   * @param string $module_id
   *   The module_id for the plugin instance.
   * @param mixed $module_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\address_decoupled\Services\AddressDecoupledInterface $address_decoupled
   *   The address decoupled service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   */
  public function __construct(
    array $config,
    $module_id,
    $module_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AddressDecoupledInterface $address_decoupled,
    LanguageManagerInterface $language_manager,
  ) {
    parent::__construct($config, $module_id, $module_definition, $serializer_formats, $logger);
    $this->addressDecoupled = $address_decoupled;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $config, $module_id, $module_definition) {
    return new static(
      $config,
      $module_id,
      $module_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('address_decoupled_rest'),
      $container->get('address_decoupled'),
      $container->get('language_manager')
    );
  }

  /**
   * Get all the supported countries from a commmerce Store entity.
   *
   * @param int $store
   *   The commerce store entity ID.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   Returns the rest response with the store's supported countries data.
   */
  public function get(int $store = NULL): ModifiedResourceResponse {
    if (empty($store) || !is_numeric($store)) {
      return new ModifiedResourceResponse(['message' => 'Could not find the store.']);
    }
    // Get current language.
    $currentLanguage = $this->languageManager->getCurrentLanguage()->getId();
    $supported_countries = $this->addressDecoupled->getStoreSupportedCountries($store, $currentLanguage);

    if (!$supported_countries) {
      return new ModifiedResourceResponse(['message' => 'There are no any supported countries for this store.']);
    }

    // Set the cache tag dependency.
    $response = new ModifiedResourceResponse($supported_countries);
    return $response;
  }

}
