<?php

namespace Drupal\address_decoupled\Plugin\rest\resource;

use Drupal\address_decoupled\Services\AddressDecoupledInterface;
use Drupal\Component\Serialization\Json;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\rest\ModifiedResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide the Address decoupled countries list resource.
 *
 * @RestResource(
 *   id = "address_decoupled_countries_list",
 *   label = @Translation("Address decoupled - Countries list"),
 *   uri_paths = {
 *     "canonical" = "/address-decoupled/api/countries-list/{country_codes}",
 *   }
 * )
 */
class AddressDecoupledCountriesListResource extends ResourceBase {

  /**
   * The address decoupled.
   *
   * @var \Drupal\address_decoupled\Services\AddressDecoupledInterface
   */
  protected AddressDecoupledInterface $addressDecoupled;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs a AddressDecoupledCountriesListResource object.
   *
   * @param array $config
   *   Config array which contains the information about the plugin instance.
   * @param string $module_id
   *   The module_id for the plugin instance.
   * @param mixed $module_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\address_decoupled\Services\AddressDecoupledInterface $address_decoupled
   *   The address decoupled service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   */
  public function __construct(
    array $config,
    $module_id,
    $module_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AddressDecoupledInterface $address_decoupled,
    LanguageManagerInterface $language_manager,
  ) {
    parent::__construct($config, $module_id, $module_definition, $serializer_formats, $logger);
    $this->addressDecoupled = $address_decoupled;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $config, $module_id, $module_definition) {
    return new static(
      $config,
      $module_id,
      $module_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('address_decoupled_rest'),
      $container->get('address_decoupled'),
      $container->get('language_manager')
    );
  }

  /**
   * Get all the countries list. Get all countries or by the given ISO codes.
   *
   * @param string $country_codes
   *   The list of the country codes into a json string format.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   Returns the rest response with the countries data.
   */
  public function get(string $country_codes = NULL): ModifiedResourceResponse {
    // Get current language.
    $currentLanguage = $this->languageManager->getCurrentLanguage()->getId();

    // Get the country codes as json string (if any).
    if (!empty($country_codes) && is_string($country_codes)) {
      $country_codes = Json::decode($country_codes);
    }
    $countries = $this->addressDecoupled->getCountriesList((array) $country_codes, $currentLanguage);

    if (!$countries) {
      return new ModifiedResourceResponse(['message' => 'There are no countries to list.']);
    }
    return new ModifiedResourceResponse($countries);
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $collection = parent::routes();
    // Add defaults for the "country_codes" parameter.
    $defaults = [
      'country_codes' => NULL,
    ];
    foreach ($collection->all() as $route) {
      $route->addDefaults($defaults);
    }
    return $collection;
  }

}
