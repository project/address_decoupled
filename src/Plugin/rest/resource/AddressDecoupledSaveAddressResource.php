<?php

namespace Drupal\address_decoupled\Plugin\rest\resource;

use Drupal\address_decoupled\Services\AddressDecoupledInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provide the Address decoupled save an address resource.
 *
 * @RestResource(
 *   id = "address_decoupled_save_address",
 *   label = @Translation("Address decoupled - Save address"),
 *   uri_paths = {
 *     "create" = "/address-decoupled/api/save-address/{address_data}/{entity_id}/{entity_type}/{field}",
 *   }
 * )
 */
class AddressDecoupledSaveAddressResource extends ResourceBase {

  use StringTranslationTrait;

  /**
   * The address decoupled.
   *
   * @var \Drupal\address_decoupled\Services\AddressDecoupledInterface
   */
  protected AddressDecoupledInterface $addressDecoupled;

  /**
   * Constructs a AddressDecoupledSaveAddressResource object.
   *
   * @param array $config
   *   Config array which contains the information about the plugin instance.
   * @param string $module_id
   *   The module_id for the plugin instance.
   * @param mixed $module_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\address_decoupled\Services\AddressDecoupledInterface $address_decoupled
   *   The address decoupled service.
   */
  public function __construct(
    array $config,
    $module_id,
    $module_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AddressDecoupledInterface $address_decoupled
  ) {
    parent::__construct($config, $module_id, $module_definition, $serializer_formats, $logger);
    $this->addressDecoupled = $address_decoupled;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $config, $module_id, $module_definition) {
    return new static(
      $config,
      $module_id,
      $module_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('address_decoupled_rest'),
      $container->get('address_decoupled')
    );
  }

  /**
   * Save an address into an existing entity.
   *
   * @param string $address_data
   *   The json string of the address data to save.
   * @param int $entity_id
   *   The target entity where to save the address to.
   * @param string $entity_type
   *   The target enttiy type to save the address to.
   * @param string $field
   *   The target entity field name.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Returns the json respose data after an address was saved or not.
   */
  public function post(string $address_data, int $entity_id, string $entity_type, string $field): JsonResponse {
    // Decode the json string from the address data into the array.
    $address_data = Json::decode($address_data);

    try {
      if ($this->addressDecoupled->saveAddress($address_data, $entity_id, $entity_type, $field)) {
        return new JsonResponse(['message' => 'The address was saved with success']);
      }
    }
    catch (\Exception $e) {
      return new JsonResponse(['message' => 'Something went wrong. Could not save the address.'], 400);
    }
    return new JsonResponse(['message' => 'Something went wrong. Could not save the address.'], 400);
  }

}
