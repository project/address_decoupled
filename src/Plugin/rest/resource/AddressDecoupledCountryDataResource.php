<?php

namespace Drupal\address_decoupled\Plugin\rest\resource;

use Drupal\address_decoupled\Services\AddressDecoupledInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide the Address decoupled country data resource.
 *
 * @RestResource(
 *   id = "address_decoupled_country_data",
 *   label = @Translation("Address decoupled - Country data"),
 *   uri_paths = {
 *     "canonical" = "/address-decoupled/api/country-data/{country_code}",
 *   }
 * )
 */
class AddressDecoupledCountryDataResource extends ResourceBase {

  /**
   * The address decoupled.
   *
   * @var \Drupal\address_decoupled\Services\AddressDecoupledInterface
   */
  protected AddressDecoupledInterface $addressDecoupled;

  /**
   * Constructs a AddressDecoupledCountryDataResource object.
   *
   * @param array $config
   *   Config array which contains the information about the plugin instance.
   * @param string $module_id
   *   The module_id for the plugin instance.
   * @param mixed $module_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\address_decoupled\Services\AddressDecoupledInterface $address_decoupled
   *   The address decoupled service.
   */
  public function __construct(
    array $config,
    $module_id,
    $module_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AddressDecoupledInterface $address_decoupled,
  ) {
    parent::__construct($config, $module_id, $module_definition, $serializer_formats, $logger);
    $this->addressDecoupled = $address_decoupled;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $config, $module_id, $module_definition) {
    return new static(
      $config,
      $module_id,
      $module_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('address_decoupled_rest'),
      $container->get('address_decoupled')
    );
  }

  /**
   * Get the country data like: subdivisions, address format, fields, etc.
   *
   * @param string $country_code
   *   The country code in ISO format to get the data from.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   Returns the rest response with the country data.
   */
  public function get(string $country_code): ModifiedResourceResponse {
    $country_data = $this->addressDecoupled->getCountryData($country_code);

    if (!$country_data) {
      return new ModifiedResourceResponse(['message' => 'There is no such country code.'], 400);
    }

    $response = new ModifiedResourceResponse($country_data);
    return $response;
  }

}
