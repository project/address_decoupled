<?php

namespace Drupal\address_decoupled\Plugin\rest\resource;

use Drupal\address_decoupled\Services\AddressDecoupledInterface;
use Drupal\Component\Serialization\Json;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide the Address decoupled validate address resource.
 *
 * @RestResource(
 *   id = "address_decoupled_validate_address",
 *   label = @Translation("Address decoupled - Validate address"),
 *   uri_paths = {
 *     "canonical" = "/address-decoupled/api/validate-address/{address_data}",
 *   }
 * )
 */
class AddressDecoupledValidateAddressResource extends ResourceBase {

  /**
   * The address decoupled.
   *
   * @var \Drupal\address_decoupled\Services\AddressDecoupledInterface
   */
  protected AddressDecoupledInterface $addressDecoupled;

  /**
   * Constructs a AddressDecoupledValidateAddressResource object.
   *
   * @param array $config
   *   Config array which contains the information about the plugin instance.
   * @param string $module_id
   *   The module_id for the plugin instance.
   * @param mixed $module_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\address_decoupled\Services\AddressDecoupledInterface $address_decoupled
   *   The address decoupled service.
   */
  public function __construct(
    array $config,
    $module_id,
    $module_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AddressDecoupledInterface $address_decoupled
  ) {
    parent::__construct($config, $module_id, $module_definition, $serializer_formats, $logger);
    $this->addressDecoupled = $address_decoupled;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $config, $module_id, $module_definition) {
    return new static(
      $config,
      $module_id,
      $module_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('address_decoupled_rest'),
      $container->get('address_decoupled')
    );
  }

  /**
   * Validate a particular address by the given format.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The rest response with the booking data.
   */
  public function get(string $address_data): ResourceResponse {
    // Decode the address data json string.
    $address_data = Json::decode($address_data);

    $validate = $this->addressDecoupled->validateAddress($address_data);

    if (!$validate) {
      return new ResourceResponse(['message' => 'The given address is a valid one.']);
    }
    return new ResourceResponse($validate, 400);
  }

}
