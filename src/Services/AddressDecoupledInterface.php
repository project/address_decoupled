<?php

declare(strict_types=1);

namespace Drupal\address_decoupled\Services;

/**
 * An interface for the AddressDecoupled service.
 */
interface AddressDecoupledInterface {

  /**
   * Get the full list of all the countries.
   *
   * @param array $iso_codes
   *   Provide the list of ISO codes to get countries list for. If no values
   *   there will be provided the full list of all the available countries.
   * @param string $locale
   *   The locale needed to get the names of the countries translated.
   *
   * @return array
   *   Returns the list of countries mapped by the country ISO code and labels.
   */
  public function getCountriesList(array $iso_codes = [], string $locale = null): array;

  /**
   * Get the country information like regions, localities, areas, etc.
   *
   * @param string $iso_code
   *   The provided country ISO code to the the information from.
   *
   * @return array
   *   Returns the country data like address format, fields, subdivisions, etc.
   */
  public function getCountryData(string $iso_code): array;

  /**
   * Validate a particular address and return the violations (if any).
   *
   * @param array $address_data
   *   The given array with the address values.
   *
   * @return array
   *   Returns the list of address validation violations (if any).
   */
  public function validateAddress(array $address_data): array;

  /**
   * Save an address for an entity.
   *
   * @param array $address_data
   *   The given array with the address values.
   * @param int $entity_id
   *   The context entity ID to save the address for.
   * @param string $entity_type
   *   The entity type of the context enttiy.
   * @param string $field
   *   The entity field name of the address field.
   */
  public function saveAddress(array $address_data, int $entity_id, string $entity_type, string $field): bool;

  /**
   * Get the commerce store supported countries list (if any).
   *
   * @param int $store_id
   *   The context store id to get the supported countries for.
   * @param string $locale
   *   The locale needed to get the names of the countries translated.
   *
   * @return array
   *   The list of the available supported countries.
   */
  public function getStoreSupportedCountries(int $store_id, string $locale = null): array;

}
