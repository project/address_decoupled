<?php

declare(strict_types=1);

namespace Drupal\address_decoupled\Services;

use CommerceGuys\Addressing\Address;
use CommerceGuys\Addressing\AddressFormat\AddressField;
use CommerceGuys\Addressing\AddressFormat\AddressFormatRepositoryInterface;
use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use CommerceGuys\Addressing\Subdivision\Subdivision;
use CommerceGuys\Addressing\Subdivision\SubdivisionRepositoryInterface;
use CommerceGuys\Addressing\Validator\Constraints\AddressFormatConstraint;
use Drupal\address\LabelHelper;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Symfony\Component\Validator\Validation;

/**
 * Service for implementation of the AddressDecoupled service.
 */
class AddressDecoupled implements AddressDecoupledInterface {

  use LoggerChannelTrait;

  /**
   * The country repository.
   *
   * @var \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  protected CountryRepositoryInterface $countryRepository;

  /**
   * The address format repository.
   *
   * @var \CommerceGuys\Addressing\AddressFormat\AddressFormatRepositoryInterface
   */
  protected AddressFormatRepositoryInterface $addressFormatRepository;

  /**
   * The subdivision repository.
   *
   * @var \CommerceGuys\Addressing\Subdivision\SubdivisionRepositoryInterface
   */
  protected SubdivisionRepositoryInterface $subdivisionRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The AddressDecoupled service constructor.
   *
   * @param \CommerceGuys\Addressing\Country\CountryRepositoryInterface $country_repository
   *   The country repository service.
   * @param \CommerceGuys\Addressing\AddressFormat\AddressFormatRepositoryInterface $address_format_repository
   *   The address format repository service.
   * @param \CommerceGuys\Addressing\Subdivision\SubdivisionRepositoryInterface $subdivision_repository
   *   The subdivision repository service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(
    CountryRepositoryInterface $country_repository,
    AddressFormatRepositoryInterface $address_format_repository,
    SubdivisionRepositoryInterface $subdivision_repository,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->countryRepository = $country_repository;
    $this->addressFormatRepository = $address_format_repository;
    $this->subdivisionRepository = $subdivision_repository;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getCountriesList(array $iso_codes = [], string $locale = null): array {
    // Get all the available countries.
    $countries = $this->countryRepository->getAll($locale);

    $result = [];

    /** @var CommerceGuys\Addressing\Country\Country[] $countries */
    foreach ($countries as $country) {
      $result[$country->getCountryCode()] = [
        'countryCode' => $country->getCountryCode(),
        'name' => $country->getName(),
        'threeLetterCode' => $country->getThreeLetterCode(),
        'numericCode' => $country->getNumericCode(),
        'currencyCode' => $country->getCurrencyCode(),
        'locale' => $country->getLocale(),
      ];
    }

    // Return only the countries filtered by the given ISO list.
    if (!empty($iso_codes)) {
      $iso_filtered_countries = [];
      foreach ($iso_codes as $iso) {
        if (!isset($result[$iso])) {
          continue;
        }
        $iso_filtered_countries[$iso] = $result[$iso];
      }
      $result = $iso_filtered_countries;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getCountryData(string $iso_code): array {
    // First, let's get the address format from the given country ISO code.
    $address_format = $this->addressFormatRepository->get($iso_code);

    $result = [];

    $country = $this->getCountriesList([$iso_code]);
    // Add a validation for the country code.
    if (!$country) {
      return $result;
    }

    // Get all the subdivision field. Usually, if there are more than 1, then
    // if means there are dependant localities inside the country.
    $subdivision_fields = $address_format->getUsedSubdivisionFields();

    $result['used_fields'] = $address_format->getUsedFields();
    $result['subdivision_fields'] = $subdivision_fields;
    $result['required_fields'] = $address_format->getRequiredFields();
    $result['uppercase_fields'] = $address_format->getUppercaseFields();
    $result['postal_code_type'] = $address_format->getPostalCodeType();
    $result['postal_code_pattern'] = $address_format->getPostalCodePattern();
    $result['postal_code_prefix'] = $address_format->getPostalCodePrefix();
    $result['field_labels'] = LabelHelper::getFieldLabels($address_format);

    $subdivisions = [];
    $subdivision = $this->subdivisionRepository->getAll([$iso_code]);

    // Make sure there are any subdivision and also any subdivision field.
    if ($subdivision && !empty($subdivision_fields)) {
      // The administrative area field key.
      $aa = reset($subdivision_fields);
      foreach ($subdivision as $subdivision_item) {
        // Get the administrative area code.
        $aa_code = $subdivision_item->getCode();
        $subdivisions[$aa][$aa_code] = $this->parseDivisionChild($subdivision_item);

        // Add the subdivision children to the result. It's the province by
        // default, so, se consider it.
        if (!$subdivision_item->hasChildren()) {
          continue;
        }
        $children = $subdivision_item->getChildren();
        $values = $children->getValues();

        foreach ($values as $subdivision_locality) {
          // Get the privince code.
          $p_code = $subdivision_locality->getCode();
          $subdivisions[$aa][$aa_code][$subdivision_fields[1]][$p_code] = $this->parseDivisionChild($subdivision_locality);

          if (!$subdivision_locality->hasChildren()) {
            continue;
          }
          $dependant_locality = $subdivision_locality->getChildren();
          $dependant_locality_values = $dependant_locality->getValues();

          foreach ($dependant_locality_values as $dependant_locality_value) {
            $subdivisions[$aa][$aa_code][$subdivision_fields[1]][$p_code][$subdivision_fields[2]][$dependant_locality_value->getCode()] = $this->parseDivisionChild($dependant_locality_value);
          }
        }
      }
    }

    $result['subdivisions'] = $subdivisions;

    return $result;
  }

  /**
   * Parse the subdivision child item into a normal readable array.
   *
   * @param \CommerceGuys\Addressing\Subdivision\Subdivision $child
   *   The address subdivision child item instance.
   *
   * @return array
   *   Return the parsed subdivision child array formatted data.
   */
  protected function parseDivisionChild(Subdivision $child): array {
    return [
      'countryCode' => $child->getCountryCode(),
      'code' => $child->getCode(),
      'localCode' => $child->getLocalCode(),
      'name' => $child->getName(),
      'localName' => $child->getLocalName(),
      'locale' => $child->getLocale(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateAddress(array $address_data): array {
    // Define a new adress instance and fill in with the provideed information.
    $address = new Address();

    // These are all the available address fields with the associated methods.
    $keys_methods = [
      'countryCode' => 'withCountryCode',
      AddressField::GIVEN_NAME => 'withGivenName',
      AddressField::ADDITIONAL_NAME => 'withAdditionalName',
      AddressField::FAMILY_NAME => 'withFamilyName',
      AddressField::ORGANIZATION => 'withOrganization',
      AddressField::ADDRESS_LINE1 => 'withAddressLine1',
      AddressField::ADDRESS_LINE2 => 'withAddressLine2',
      AddressField::POSTAL_CODE => 'withPostalCode',
      AddressField::SORTING_CODE => 'withSortingCode',
      AddressField::DEPENDENT_LOCALITY => 'withDependentLocality',
      AddressField::LOCALITY => 'withLocality',
      AddressField::ADMINISTRATIVE_AREA => 'withAdministrativeArea',
    ];

    foreach ($keys_methods as $key => $method) {
      if (!empty($address_data[$key])) {
        $address = $address->{$method}($address_data[$key]);
      }
    }

    // Prepare the validatod instance and then use the provided address to
    // sanitize it and get all the possible violations (if any).
    $validator = Validation::createValidator();
    $violations = $validator->validate($address, new AddressFormatConstraint());

    $result = [];
    foreach ($violations as $violation) {
      $field_key = str_replace(['[', ']'], ['', ''], $violation->getPropertyPath());
      $result[$field_key] = $violation->getMessage();
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function saveAddress(array $address_data, int $entity_id, string $entity_type, string $field): bool {
    /** @var \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\ContentEntityInterface|null $entity */
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);

    // Could not find an entiy by the given context.
    if (!$entity) {
      return FALSE;
    }

    // There is no such field in the entity data model.
    if (!$entity->{$field}) {
      return FALSE;
    }

    // Map all the address keys with the drupal field keys.
    $keys = [
      'countryCode' => 'country_code',
      AddressField::GIVEN_NAME => 'given_name',
      AddressField::ADDITIONAL_NAME => 'additional_name',
      AddressField::FAMILY_NAME => 'family_name',
      AddressField::ORGANIZATION => 'organization',
      AddressField::ADDRESS_LINE1 => 'address_line1',
      AddressField::ADDRESS_LINE2 => 'address_line2',
      AddressField::POSTAL_CODE => 'postal_code',
      AddressField::SORTING_CODE => 'sorting_code',
      AddressField::DEPENDENT_LOCALITY => 'dependent_locality',
      AddressField::LOCALITY => 'locality',
      AddressField::ADMINISTRATIVE_AREA => 'administrative_area',
    ];

    try {
      // Format address data into the drupal field data.
      $data = [];
      foreach ($keys as $address_key => $field_key) {
        $data[$field_key] = $address_data[$address_key];
      }
      $entity->set($field, $data);
      $entity->save();
    }
    catch (\Exception $e) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getStoreSupportedCountries(int $store_id, string $locale = null): array {
    $store = $this->entityTypeManager->getStorage('commerce_store')->load($store_id);

    if (!$store instanceof StoreInterface) {
      return [];
    }

    if ($store->hasField('shipping_countries')) {
      $shipping_countries = array_column($store->get('shipping_countries')->getValue(), 'value');
      $shipping_countries = $this->getCountriesList($shipping_countries, $locale);
    }
    if ($store->hasField('billing_countries')) {
      $billing_countries = array_column($store->get('billing_countries')->getValue(), 'value');
      $billing_countries = $this->getCountriesList($billing_countries, $locale);
    }

    return [
      'shipping_countries' => $shipping_countries ?? [],
      'billing_countries' => $billing_countries ?? [],
    ];
  }

}
